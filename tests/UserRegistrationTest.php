<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class UserRegistrationTest extends KernelTestCase
{
    public function testNewUserRegistration()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:create-user');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['JohnDoe', 'johndoe@mail.com']);
        $commandTester->execute([]);

        $this->assertStringContainsString('User created successfully', $commandTester->getDisplay());
    }

    public function testRegistrationWithoutName()
    {
        $this->expectException(\RuntimeException::class);

        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:create-user');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['', 'johndoe@mail.com']);
        $commandTester->execute([]);
    }

    public function testrRegistrationWithoutEmail()
    {
        $this->expectException(\RuntimeException::class);

        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:create-user');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['JohnDoe', '']);
        $commandTester->execute([]);
    }
}
