<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AccessTest extends WebTestCase
{
    public function testShowHomePage()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testAccessLoginPage()
    {
        $client = static::createClient();

        $client->request('GET', '/login');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('html form button', 'Login');
    }

    public function testLoginActionWithEmail()
    {
        $client = static::createClient();
        $request = $client->request('GET', '/');

        $form = $request->selectButton('Login')->form();
        $form['email']->setValue('admin@mail.com');

        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains(
            'html body',
            'Redirecting to',
            $client->getResponse()->getContent()
        );
    }
}
