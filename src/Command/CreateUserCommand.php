<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class CreateUserCommand extends Command
{

    private $entityManager;

    protected static $defaultName = 'app:create-user';

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create a new user.')
            ->setHelp('This command allows you to create a new user...')
            ->addOption('fullname', null,InputOption::VALUE_REQUIRED, 'Fullname', null)
            ->addOption('email', null,InputOption::VALUE_REQUIRED, 'Email', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $fullname = $input->getOption('fullname');
        $email = $input->getOption('email');

        $output->writeln([
            'User Creation',
            '=========================',
            '',
        ]);

        $helper = $this->getHelper('question');

        $question = new Question('Fullname: ');
        $question->setValidator(function ($value) {
            if (trim($value) == '') {
                throw new \Exception('Fullname cannot be empty');
            }

            return trim($value);
        });

        $fullname = $helper->ask($input, $output, $question);

        $question = new Question('Email: ');
        $question->setValidator(function ($value) {
            if ($value == '') {
                throw new \Exception('Email cannot be empty');
            }
            $this->emailValidation($value);

            return $value;
        });

        $email = $helper->ask($input, $output, $question);

        $user = new User();
        $user->setFullName($fullname);
        $user->setEmail($email);
        $user->setRoles(["ROLE_USER"]);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $io->success('User created successfully: ' . $user->getId());

        return 0;
    }

    /**
     * @param string $email
     *
     */
    private function emailValidation(string $email): void
    {
        $validator = Validation::createValidator();

        $violations = $validator->validate($email, [
            new NotBlank(),
            new Email()
        ]);

        if (!is_string($email) || count($violations) !== 0) {
            throw new \RuntimeException($violations[0]->getMessage());
        }
    }
}
