<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullName;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\OneToMany(targetEntity=Teammate::class, mappedBy="user", cascade={"remove"})
     */
    private $teammates;

    public function __construct()
    {
        $this->teammates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTeammate(): ?Teammate
    {
        return $this->teammate;
    }

    public function setTeammate(?Teammate $teammate): self
    {
        $this->teammate = $teammate;

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword()
    {
        // Implement getPassword() method needed.
    }

    public function getSalt()
    {
        // Implement getSalt() method needed.
    }

    public function eraseCredentials()
    {
        // Implement eraseCredentials() method needed.
    }

    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @return Collection|Teammate[]
     */
    public function getTeammates(): Collection
    {
        return $this->teammates;
    }

    public function addTeammate(Teammate $teammate): self
    {
        if (!$this->teammates->contains($teammate)) {
            $this->teammates[] = $teammate;
            $teammate->setUser($this);
        }

        return $this;
    }

    public function removeTeammate(Teammate $teammate): self
    {
        if ($this->teammates->removeElement($teammate)) {
            // set the owning side to null (unless already changed)
            if ($teammate->getUser() === $this) {
                $teammate->setUser(null);
            }
        }

        return $this;
    }
}
