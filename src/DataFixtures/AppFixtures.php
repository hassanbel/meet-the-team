<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {


        // Create our user and set details
        $admin = new User();
        $admin->setFullName('admin');
        $admin->setEmail('admin@mail.com');
        $admin->setRoles(array('ROLE_ADMIN'));

        $manager->persist($admin);
        $manager->flush();
    }
}
