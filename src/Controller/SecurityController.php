<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Validator\Constraints\Email as EmailConst;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use DateInterval;

class SecurityController extends AbstractController
{

    private $session;
    private $mailer;
    private $validator;
    private $cache;

    public function __construct(SessionInterface $session, MailerInterface $mailer, ValidatorInterface $validator, AdapterInterface $cache)
    {
        $this->session = $session;
        $this->mailer = $mailer;
        $this->validator = $validator;
        $this->cache = $cache;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('teammate_index');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $email = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['email' => $email, 'error' => $error]);
    }

    /**
     * @Route("/identified", name="identified_user")
     */
    public function identifiedUser(Request $request)
    {
        $csrfToken = $request->request->get('_csrf_token');
        $email = $request->request->get('email');
        $password = md5(uniqid());

        if (!$this->isCsrfTokenValid('authenticate', $csrfToken)) {
            throw new InvalidCsrfTokenException();
        }

        $this->validator->validate($email, [
            new NotBlank(),
            new EmailConst(),
        ]);

        $cacheItem = md5($email);
        $item = $this->cache->getItem($cacheItem);
        $item->set($password);

        $item->expiresAfter(new DateInterval('PT1M'));
        $this->cache->save($item);

        $this->session->set('identified_user_email', $email);

        $emailContent = (new Email())
            ->from('noreply@meettheteam.com')
            ->to($email)
            ->subject('Auth email')
            ->html('<p>Hello, <br> You logged in with email: '. $email .' </p><p>Your Password is: '. $password .'</p><br><b>Thank you.</b>');

        $this->mailer->send($emailContent);

        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): Response
    {
        // Lougout method
    }
}
